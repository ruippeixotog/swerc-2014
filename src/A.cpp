#include <cstring>
#include <iostream>
#include <string>

#define MAXN 10

using namespace std;

int n;
string words[MAXN];

bool used[10];
int letterValue[26];
int getValue(int i, int d) { return letterValue[words[i][words[i].length() - d - 1] - 'A']; }
void setValue(int i, int d, int value) {
  if(value == -1) used[getValue(i, d)] = false;
  else used[value] = true;
  letterValue[words[i][words[i].length() - d - 1] - 'A'] = value;
}

// we are currently in the d'th char (right to left) of word i
int solve(int d, int i, int sum) {

  // if the char does not exist
  if(d >= words[i].length()) {
    if(i == n - 1) return sum == 0 ? 1 : 0;
    return solve(d, i + 1, sum);
  }

  int chValue = getValue(i, d);

  // if the char already has a value assigned
  if(chValue >= 0) {
    if(d == words[i].length() - 1 && chValue == 0) return 0;
    
    if(i == n - 1) return chValue == sum % 10 ? solve(d + 1, 0, sum / 10) : 0;
    return solve(d, i + 1, sum + chValue);
  }

  // if the char does not have a value assigned and this is the last word
  if(i == n - 1) {
    if(used[sum % 10]) return 0;
    if(d == words[i].length() - 1 && sum % 10 == 0) return 0;

    setValue(i, d, sum % 10);
    int res = solve(d + 1, 0, sum / 10);
    setValue(i, d, -1);
    return res;
  }

  // if the char does not have a value assigned and this is not the last word
  int first = (d == words[i].length() - 1 ? 1 : 0);
  int res = 0;
  for(int v = first; v <= 9; v++) {
    if(used[v]) continue;
    setValue(i, d, v);
    res += solve(d, i + 1, sum + v);
    setValue(i, d, -1);
  }
  return res;
}

int main() {
  while(cin >> n) {
    for(int i = 0; i < n; i++)
      cin >> words[i];

    memset(letterValue, -1, sizeof(letterValue));
    cout << solve(0, 0, 0) << endl;
  }
  return 0;
}
