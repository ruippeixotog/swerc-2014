#include <cstring>
#include <iostream>
#include <queue>
#include <vector>

#define MAXN 10000
#define MAXV (MAXN * 2)
#define INF 1e9

using namespace std;

int n;
vector<int> adjs[MAXV];

int from[MAXV], to[MAXV];

int dist[MAXV];
int nilDist;
inline int getDist(int i) { return i == -1 ? nilDist : dist[i]; }
inline void setDist(int i, int d) { if(i == -1) nilDist = d; else dist[i] = d; }

bool bfs() {
  queue<int> q;

  setDist(-1, INF);
  for(int i = 0; i < n; i++) {
    if(from[i] == -1) { setDist(i, 0); q.push(i); }
    else setDist(i, INF);
  }

  while(!q.empty()) {
    int curr = q.front(); q.pop();
    if(getDist(curr) >= getDist(-1)) continue;

    for(int i = 0; i < adjs[curr].size(); i++) {
      int adj = adjs[curr][i];

      if(getDist(to[adj]) == INF) {
        setDist(to[adj], dist[curr] + 1);
        q.push(to[adj]);
      }
    }
  }
  return getDist(-1) != INF;
}

bool dfs(int curr) {
  if(curr == -1) return true;
  for(int i = 0; i < adjs[curr].size(); i++) {
    int adj = adjs[curr][i];
    if(getDist(to[adj]) == getDist(curr) + 1 && dfs(to[adj])) {
      from[curr] = adj;
      to[adj] = curr;
      return true;
    }
  }
  setDist(curr, INF);
  return false;
}

int main() {
  int m;
  while(cin >> n >> m) {
    while(m--) {
      int a, b; cin >> a >> b;
      adjs[a].push_back(b + MAXN);
      adjs[b + MAXN].push_back(a);
    }

    memset(from, -1, sizeof(from));
    memset(to, -1, sizeof(to));

    int matchCount = 0;

    while(bfs()) {
      cerr << "BFS Iter" << endl;
      cerr << "From: ";
      for(int i = 0; i < n; i++) cerr << from[i] << " ";
      cerr << endl << "To  : ";
      for(int i = 0; i < n; i++) cerr << to[i+MAXN] << " ";
      cerr << endl;

      for(int i = 0; i < n; i++) {
        if(from[i] == -1 && dfs(i)) matchCount++;
      }
    }

    cout << (matchCount == n ? "YES" : "NO") << endl;
    return 0;
  }
}
