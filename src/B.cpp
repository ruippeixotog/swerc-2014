#include <cstring>
#include <iostream>
#include <queue>
#include <utility>
#include <vector>

#define INF 0x3f
#define MAXN 10000

using namespace std;

int graph[MAXN][MAXN];
vector<pair<int, int> > adjs[MAXN];

bool visited[MAXN];
int dist[MAXN];
vector<int> last[MAXN];

bool lenCounted[MAXN];
int totalLen = 0;

void dfs(int i) {
  if(lenCounted[i]) return;
  lenCounted[i] = true;

  for(int j = 0; j < last[i].size(); j++) {
    totalLen += graph[i][last[i][j]] * 2;
    dfs(last[i][j]);
  }
}

int main() {
  int p, t; cin >> p >> t;
  for(int i = 0; i < t; i++) {
    int p1, p2, l; cin >> p1 >> p2 >> l;
    if(graph[p1][p2] == 0 || l < graph[p1][p2]) {
      graph[p1][p2] = graph[p2][p1] = l;
    }
    adjs[p1].push_back(make_pair(p2, l));
    adjs[p2].push_back(make_pair(p1, l));
  }

  memset(dist, INF, sizeof(dist));
  priority_queue<pair<int, int> > q;
  dist[0] = 0; q.push(make_pair(0, 0));

  while(!q.empty()) {
    pair<int, int> curr = q.top(); q.pop();
    int currNode = curr.second, currDist = -curr.first;

    if(visited[currNode]) continue;
    visited[currNode] = true;
    if(currNode == p - 1) break;

    for(int i = 0; i < adjs[currNode].size(); i++) {
      pair<int, int> adj = adjs[currNode][i];

      if(currDist + adj.second < dist[adj.first]) {
        dist[adj.first] = currDist + adj.second;
        last[adj.first].clear();
        last[adj.first].push_back(currNode);
        q.push(make_pair(-dist[adj.first], adj.first));

      } else if(currDist + adj.second == dist[adj.first]) {
        last[adj.first].push_back(currNode);
      }
    }
  }

  memset(lenCounted, 0, sizeof(lenCounted));
  dfs(p - 1);

  cout << totalLen << endl;
  return 0;
}
